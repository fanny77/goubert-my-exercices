import {my_size_alpha} from '../day-1/exercice-4.js';

export const my_alpha_reverse = (str) => {
	let nbr = my_size_alpha(str);
	let alpha_reverse = [];

	for (let i = nbr - 1 ; i >= 0; i--) {
		alpha_reverse = alpha_reverse + str[i];
	}
	return alpha_reverse;
}