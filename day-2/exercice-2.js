//Faite en sorte de retourner true si la chaine passé en paramètre contient un nombre.
//Si tout autre chose comme résultat renvoyez false.

export const my_display_multi_42 = (multi,int) => {

	if (typeof multi == "number" && typeof int == "number") {
		if (multi*int == 42) {
			return true;
		}
		else{
			return false;
		}
	}
	else{
		return false
	}
}