export const my_string_is_number = (str) => {

	for (let i = 0; i < str.length; i++) {
		if (parseInt(str[i],10)) {
			return true
		}
	}
	return false
}