import {my_display_alpha} from './exercice-1.js';
import {my_size_alpha} from './exercice-4.js';

export const my_display_alpha_reverse = () => {
	let alpha = my_display_alpha();
	let nbr = my_size_alpha(alpha);
	let alpha_reverse = [];

	for (let i = nbr - 1 ; i >= 0; i--) {
		alpha_reverse = alpha_reverse + alpha[i];
	}
	return alpha_reverse;
}