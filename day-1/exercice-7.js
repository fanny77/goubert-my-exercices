export const my_is_posi_neg = (nbr) => {
	if (nbr < 0) {
		return "NEGATIF";
	}

	if (nbr == 0){
		return "NEUTRAL";
	}

	return "POSITIF";
}
console.log(my_is_posi_neg(15));
console.log(my_is_posi_neg(0));
console.log(my_is_posi_neg(-13));