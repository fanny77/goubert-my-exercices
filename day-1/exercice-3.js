export const my_alpha_number = (nbr) => {
	let entier = Math.floor(nbr);
	let unite = entier % 10;
	let char = "";
	switch (unite) {
		case 0: char = "0"; break
		case 1: char = "1"; break
		case 2: char = "2"; break
		case 3: char = "3"; break
		case 4: char = "4"; break
		case 5: char = "5"; break
		case 6: char = "6"; break
		case 7: char = "7"; break
		case 8: char = "8"; break
		case 9: char = "9"; break
	}
	if (entier >= 10) {
		return my_alpha_number(entier/10) + char;
	}
	return char;
}


